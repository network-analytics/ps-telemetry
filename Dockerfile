FROM centos:7

LABEL maintainer "Marian Babik <Marian.Babik@cern.ch>"
LABEL description "ps-telemetry image"
LABEL version "0.2"

RUN yum -y install epel-release
RUN yum -y install python-messaging python-dirq
RUN yum -y install python-devel cython python-pip gcc-c++ atlas-devel gcc-gfortran hdf5-devel git
RUN pip install --upgrade pip
RUN pip install  numpy==1.11.1 pandas==0.18.1 scipy==0.18.0 h5py theano keras
RUN ln -s /usr/lib64/atlas/libsatlas.so /usr/lib64/atlas/libcblas.so
RUN pip install scikit-learn==0.17.1

RUN git clone https://github.com/marian-babik/netTelD.git
RUN bash /netTelD/post_install.sh

WORKDIR /netTelD/netTelD/
ENTRYPOINT ["/usr/bin/python", "netTel.py"]
