#!/bin/bash
set -e

echo "================================================"
echo "ps-telemetry version: 0.0.01 Copyright CERN 2016"
echo "License: https://github.com/HenniOVP/netTelD/blob/master/LICENSE"
echo ""

echo "Starting messaging clients ..."
/usr/bin/stompclt --conf /etc/stompclt/stompclt_netTel_input_mb105.cfg --pidfile /var/run/stompclt_netTel_input_mb105.pid --daemon
/usr/bin/stompclt --conf /etc/stompclt/stompclt_netTel_input_mb202.cfg --pidfile /var/run/stompclt_netTel_input_mb202.pid --daemon
/usr/bin/stompclt --conf /etc/stompclt/stompclt_netTel_output.cfg --pidfile /var/run/stompclt_netTel_output.pid --daemon

echo "Starting ps-telemetry ..."
cd /netTelD/netTelD/
/usr/bin/python /netTelD/netTelD/netTel.py



